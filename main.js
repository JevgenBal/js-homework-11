let passwordFields = document.querySelectorAll('input[type="password"]');
let icons = document.querySelectorAll('.icon-password');

icons.forEach((icon, index) => {
  icon.addEventListener('click', () => {
    let passwordField = passwordFields[index];
    let iconClasses = icon.classList;

    if (passwordField.type === 'password') {
      passwordField.type = 'text';
      iconClasses.remove('fa-eye');
      iconClasses.add('fa-eye-slash');
      icon.setAttribute('aria-label', 'Hide password');
    } else {
      passwordField.type = 'password';
      iconClasses.add('fa-eye');
      iconClasses.remove('fa-eye-slash');
      icon.setAttribute('aria-label', 'Show password');
    }
  });
});

let form = document.querySelector('.password-form');
form.addEventListener('submit', (event) => {
  event.preventDefault();
  
  const firstPasswordField = passwordFields[0];
  const secondPasswordField = passwordFields[1];

  if (firstPasswordField.value === secondPasswordField.value) {
    alert('You are welcome!');
  } else {
    let errorMessage = form.querySelector('.error-message');
    if (errorMessage) {
      errorMessage.textContent = 'Потрібно ввести однакові значення!';
    } else {
      errorMessage = document.createElement('p');
      errorMessage.textContent = 'Потрібно ввести однакові значення!';
      errorMessage.style.color = 'red';
      errorMessage.classList.add('error-message');
      form.appendChild(errorMessage);
    }
  }
});

passwordFields.forEach((passwordField, index) => {
  passwordField.addEventListener('input', () => {
    let errorMessage = form.querySelector('.error-message');
    if (errorMessage) {
      errorMessage.remove();
    }
  });
});
